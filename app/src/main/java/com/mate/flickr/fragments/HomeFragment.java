package com.mate.flickr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mate.flickr.R;
import com.mate.flickr.adapter.GridViewAdapter;
import com.mate.flickr.appController.AppController;
import com.mate.flickr.model.Item;
import com.mate.flickr.utils.Constants;
import com.mate.flickr.utils.Globalvariables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by sasuke on 8/6/16.
 */
public class HomeFragment extends Fragment {

    private static final String TAG = HomeFragment.class.getSimpleName();

    private static GridView gridView;
    private static List<Item> itemList;
    private static GridViewAdapter adapter;
    private static HashMap<Integer, Boolean> isFrontFacing = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        gridView = (GridView) rootView.findViewById(R.id.grid_view);

        return rootView;
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        //Local variables

        Log.e(TAG, "User ID: " + Globalvariables.userID);

        itemList = getPhotos();

        Log.e(TAG, "Item list size: " + itemList.size());

        //adapter
        adapter = new GridViewAdapter(getActivity(), itemList);

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {

                final ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
                final TextView des = (TextView) view.findViewById(R.id.des);

                final Animation animation1 = AnimationUtils.loadAnimation(getContext(), R.anim.to_middle);
                final Animation animation2 = AnimationUtils.loadAnimation(getContext(), R.anim.from_middle);

                view.setAnimation(animation1);
                view.startAnimation(animation1);

                animation1.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        if(animation == animation1) {

                            if(isFrontFacing.get(position)) {

                                imageView.setVisibility(View.GONE);
                                des.setVisibility(View.VISIBLE);

                                isFrontFacing.put(position, false);
                            } else {

                                imageView.setVisibility(View.VISIBLE);
                                des.setVisibility(View.GONE);

                                isFrontFacing.put(position, true);
                            }

                            view.clearAnimation();
                            view.setAnimation(animation2);
                            view.startAnimation(animation2);
                        }else {

                            isFrontFacing.put(position, false);
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        adapter.notifyDataSetChanged();
    }

    public List<Item> getPhotos() {

        final List<Item> itemList = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.FLICKR_REST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e(TAG, "Ressponse: " + response);

                String responseString = response.substring(14, response.length());

                Log.e(TAG, "Response String: "  + responseString);

                try {

                    JSONObject jsonObject = new JSONObject(responseString);

                    String stat = jsonObject.getString("stat");

                    if(stat.equals("ok")) {

                        JSONObject photos = jsonObject.getJSONObject("photos");

                        //Log.e(TAG, "Photos: " + photos);

                        JSONArray photo = photos.getJSONArray("photo");

                        Log.e(TAG, "photo: " + photo);

                        for (int i=0; i<photo.length(); i++) {

                            JSONObject photoJsonObject = photo.getJSONObject(i);

                            Log.e(TAG, "Photos: " + photoJsonObject);

                            Item item = new Item();
                            item.setImageURL("https://farm" + photoJsonObject.getString("farm") + ".staticflickr.com/" +
                                    photoJsonObject.getString("server") + "/" +
                                    photoJsonObject.getString("id") + "_" +
                                photoJsonObject.getString("secret") + ".jpg");
                            item.setDes(photoJsonObject.getString("title"));

                            isFrontFacing.put(i, true);

                            Log.e(TAG, "photo url: " + item.getImageURL());

                            itemList.add(item);

                            adapter.notifyDataSetChanged();

                        }
                    }else {

                        Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("api_key", Constants.API_KEY);
                params.put("method", Constants.PHOTOS_SEARCH_METHOD);
                params.put("tags", "hiking");
                params.put("format", "json");
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);

        return itemList;
    }

}
