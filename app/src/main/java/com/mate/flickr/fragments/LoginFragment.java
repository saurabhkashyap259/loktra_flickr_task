package com.mate.flickr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mate.flickr.MainActivity;
import com.mate.flickr.R;
import com.mate.flickr.appController.AppController;
import com.mate.flickr.utils.Constants;
import com.mate.flickr.utils.Globalvariables;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sasuke on 10/6/16.
 */
public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    public static EditText username;
    public static Button getPhotos;
    public static String inputUsername;;

    public LoginFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        username = (EditText) rootView.findViewById(R.id.username);
        getPhotos = (Button) rootView.findViewById(R.id.get_photos);

        return rootView;
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        //Local variables

        getPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get the username
                inputUsername = username.getText().toString();

                if(inputUsername.trim().length() > 0) {

                    getUserID();
                } else {

                    Toast.makeText(getActivity(), "Please enter the username", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void getUserID() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.FLICKR_REST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e(TAG, "Response: " + response);

                Pattern p = Pattern.compile("\\(.*?\\)");
                Matcher m = p.matcher(response);
                String responseString = null;

                if(m.find()) {

                    responseString = m.group().subSequence(1, m.group().length()-1).toString();
                }

                try {
                    JSONObject jsonObject = new JSONObject(responseString);

                    String stat = jsonObject.getString("stat");

                    if(stat.equals("ok")) {

                        JSONObject user = jsonObject.getJSONObject("user");

                        Globalvariables.userID = user.getString("id");

                        MainActivity.openFragment(Constants.HomeFragmentCount);
                    }else {

                        Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e(TAG, "Volley Error: " + error);
            }
        })
        {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("method", Constants.USERNAME_METHOD);
                params.put("api_key", Constants.API_KEY);
                params.put("username", inputUsername);
                params.put("format", "json");
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
