package com.mate.flickr.model;

/**
 * Created by sasuke on 8/6/16.
 */
public class Item {

    String imageURL;
    String des;

    public Item() {
    }

    public Item(String imageURL, String des) {
        this.imageURL = imageURL;
        this.des = des;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getDes() {
        return des;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
