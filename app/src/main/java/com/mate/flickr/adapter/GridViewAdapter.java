package com.mate.flickr.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.mate.flickr.R;
import com.mate.flickr.appController.AppController;
import com.mate.flickr.model.Item;
import com.mate.flickr.viewHolder.ItemViewHolder;

import java.util.List;

/**
 * Created by sasuke on 9/6/16.
 */
public class GridViewAdapter extends BaseAdapter{

    private Activity activity;
    private LayoutInflater inflater;
    private List<Item> itemList;
    private ImageLoader imageLoader;

    public GridViewAdapter(Activity activity, List<Item> itemList) {

        this.activity = activity;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ItemViewHolder holder = new ItemViewHolder();

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        if (row == null) {
            row = inflater.inflate(R.layout.card_view_items, parent, false);
            // initialize the elements
            holder.imageView = (NetworkImageView) row.findViewById(R.id.image_view);
            holder.des = (TextView) row.findViewById(R.id.des);

            row.setTag(holder);
        } else {
            holder = (ItemViewHolder) row.getTag();
        }

        // getting catalog data for the row
        Item i = itemList.get(position);

        if (i != null) {

            holder.imageView.setImageUrl(i.getImageURL(), imageLoader);

            holder.des.setText(i.getDes());
        }

        return row;
    }
}
