package com.mate.flickr.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.mate.flickr.R;

/**
 * Created by sasuke on 8/6/16.
 */
public class ItemViewHolder {

    public NetworkImageView imageView;
    public TextView des;
}
