package com.mate.flickr.utils;

/**
 * Created by sasuke on 8/6/16.
 */
public class Constants {

    public static final int HomeFragmentCount = 500;
    public static final int LoginFragmentCount = 501;

    public static final String FLICKR_REST_URL = "https://api.flickr.com/services/rest/";
    public static final String FLICKR_PHOTO_URL = "https://www.flickr.com/photos/";
    public static final String PHOTOS_SEARCH_METHOD = "flickr.photos.search";


    public static final String API_KEY = "e7872a93d722a17ecb615c94accc99a7";
    public static final String API_SECRET = "79f60dceafbf9c51";

    public static final String USERNAME_METHOD = "flickr.people.findByUsername";
    public static final String PHOTOS_METHOD = "flickr.people.getPublicPhotos";
}
